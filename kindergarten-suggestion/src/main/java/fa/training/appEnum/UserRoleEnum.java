package fa.training.appEnum;

public enum UserRoleEnum {
    PARENTS, ADMIN, SCHOOL_OWNER
}
