package fa.training.repository;

import fa.training.entities.User;

import java.util.Optional;

public interface UserRepository extends BaseRepository<User, Integer> {
    Optional<User> findByAccount_UsernameAndDeletedFalse(String username);
}
