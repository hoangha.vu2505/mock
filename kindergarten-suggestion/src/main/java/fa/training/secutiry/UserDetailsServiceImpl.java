package fa.training.secutiry;

import fa.training.entities.User;
import fa.training.repository.UserRepository;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Optional;

@Component
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserRepository userRepository;

    public UserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> userOptional = userRepository.findByAccount_UsernameAndDeletedFalse(username);
        if (userOptional.isEmpty()) {
            throw new UsernameNotFoundException("Username is not exist!");
        }
        User user = userOptional.get();
        return new org.springframework.security.core.userdetails.User(
                user.getAccount().getUsername(),
                user.getAccount().getPassword(),
                Collections.singletonList(new SimpleGrantedAuthority("ROLE_" + user.getRole().name()))
        );
    }
}
