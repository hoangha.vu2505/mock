package fa.training.secutiry;

import fa.training.appEnum.UserRoleEnum;
import fa.training.exception.UnAuthorizedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class SecurityUtil {
    public static Optional<String> getCurrentUserOpt() {
        Authentication authentication =
                SecurityContextHolder.getContext().getAuthentication();
        Object object = authentication.getPrincipal();
        if (object instanceof User) {
            return Optional.of(((User) object).getUsername());
        }
        return Optional.empty();
    }

    public static String getCurrentUser() {
        return getCurrentUserOpt().orElseThrow(UnAuthorizedException::new);
    }

    public static Optional<UserRoleEnum> getRoleCurrentUserOpt() {
        Authentication authentication =
                SecurityContextHolder.getContext().getAuthentication();
        return authentication.getAuthorities().stream()
                .map(authority -> UserRoleEnum.valueOf(
                        authority.getAuthority().replaceAll("ROLE_", "")))
                .findFirst();
    }

    public static UserRoleEnum getRoleCurrentUser() {
        return getRoleCurrentUserOpt().orElseThrow(UnAuthorizedException::new);
    }

    public static boolean isAdmin() {
        return getRoleCurrentUser() == UserRoleEnum.ADMIN;
    }

    public static boolean isSchoolOwner() {
        return getRoleCurrentUser() == UserRoleEnum.SCHOOL_OWNER;
    }
    public static boolean isParents() {
        return getRoleCurrentUser() == UserRoleEnum.PARENTS;
    }


}
