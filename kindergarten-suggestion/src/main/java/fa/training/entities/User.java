package fa.training.entities;

import fa.training.appEnum.UserRoleEnum;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToOne;
import lombok.*;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User extends BaseEntity {
    private String fullName;
    private UserRoleEnum role;

    @OneToOne
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Account account;
}
